﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Gaseosa:Bebida
    {
        public decimal PorcentajeAzucar { get; set; }
        public bool TieneGas { get; set; } //true si
        public bool TienePromocion { get; set; } //true si

        public override double CalcularCostoFabricacion()
        {
            if (PorcentajeAzucar>=75)
            {
                return ((55*Precio)/100);
            }
            else
            {
                return ((50*Precio)/100);
            }
        }

        public override double CalcularPrecioConIVA()
        {
            return Precio + ((Precio * 10.5) / 100);
        }
    }
}
