﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class AguaMineral:Bebida
    {
        public string Origen { get; set; }
        public decimal PorcentajePureza { get; set; }

        public override double CalcularCostoFabricacion()
        {
            if (PorcentajePureza==100)
            {
                return ((33 * Precio) / 100);
            }
            else
            {
                return ((30 * Precio) / 100);
            }
        }

        public override double CalcularPrecioConIVA()
        {
            return Precio + ((Precio * 10.5) / 100);
        }
    }
}
