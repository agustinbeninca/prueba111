﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Alcoholica:Bebida
    {
        public decimal PorcentajeAlcohol { get; set; }
        public bool NacionalImportada { get; set; } //true nacional

        public override double CalcularCostoFabricacion()
        {
            return ((45*Precio)/100);
        }

        public override double CalcularPrecioConIVA()
        {
            return Precio + ((Precio * 21) / 100);
        }

        
    }
}
