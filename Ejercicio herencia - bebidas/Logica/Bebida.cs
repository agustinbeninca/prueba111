﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Bebida
    {
        public int Identificador { get; set; }
        public string Nombre { get; set; }
        public decimal Litros { get; set; }
        public double Precio { get; set; }
        public string Marca { get; set; }

        public abstract double CalcularCostoFabricacion();
        public abstract double CalcularPrecioConIVA();
    }
}
