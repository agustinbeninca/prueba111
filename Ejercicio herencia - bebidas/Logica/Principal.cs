﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Bebida> ListaBebidas = new List<Bebida>();
        public void ObtenerListaBebidasPremium()
        {
            foreach (var item in ListaBebidas)
            {
                Bebida bebida = item;
                bool esAlcoholica = bebida is Alcoholica;
                if (esAlcoholica == true)
                {
                    if (bebida.Litros >= 1 && bebida.NacionalImportada == false)
                    {
                        BebidasPremium.Add(item);
                    }

                }
            }            
        }
    }
}
